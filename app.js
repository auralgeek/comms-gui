const express = require('express')
const path = require('path')
const zmq = require("zeromq")
const cbor = require("cbor")
const app = express()
const client_port = 3000

// Globals
var client_res = null

app.use(express.static(path.join(__dirname, 'public/images')))
app.use(express.static(path.join(__dirname, 'public/stylesheets')))
app.use(express.static(path.join(__dirname, 'public/javascripts')))
app.use(express.urlencoded())

// Set up ZMQ connection listening for new data
const sock = new zmq.Pull
const zmq_port = 57324
var zmq_addr = "tcp://127.0.0.1:" + zmq_port.toString()
sock.connect(zmq_addr)
console.log("Server listening on port " + zmq_port.toString() + " for ZMQ Push traffic")
async function zmq_listener() {
  for await (const [msg] of sock) {
    const [msg] = await sock.receive()
    if (client_res) {
      let data_packet = "data:"
      cbor.decodeFirst(msg, function(err, obj) {
        if (err != null) {
          console.log("CBOR Decode Error: " + err)
        } else {
          console.log("Got data, pushing to client")
          data_packet = data_packet + "," + obj.toString() + "\n\n"
          client_res.write(data_packet)
        }
      });
    } else {
      console.log("Got data, but no client, dropping")
    }
  }
}

zmq_listener()

// Routing
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/views/index.html'))
})
app.get('/const', (req, res) => {
  res.sendFile(path.join(__dirname + '/views/constellation_plot.html'))
})
app.get('/sigplotconst', (req, res) => {
  res.sendFile(path.join(__dirname + '/views/vue_sigplot_const.html'))
})
app.get('/sigplottime', (req, res) => {
  res.sendFile(path.join(__dirname + '/views/vue_time_plot.html'))
})
app.get('/sigplotwaterfall', (req, res) => {
  res.sendFile(path.join(__dirname + '/views/vue_waterfall.html'))
})
app.post('/zmqconnect', (req, res) => {
  console.log("ZMQ disconnecting from " + zmq_addr)
  sock.disconnect(zmq_addr)
  const ipaddr = req.body.ipaddr
  const port = req.body.port
  console.log(req.body)
  zmq_addr = 'ipc://' + ipaddr.toString() + ":" + port.toString()
  sock.connect(zmq_addr)
  console.log("ZMQ connected to " + zmq_addr)
  res.redirect('/')
})
app.get('/data-server', async function(req, res) {
  res.status(200).set({
    "connection": "keep-alive",
    "cache-control": "no-cache",
    "content-type": "text/event-stream",
  })

  // Heartbeat
  const nln = function() {
    res.write('\n')
  }
  const hbt = setInterval(nln, 15000);

  client_res = res

  req.on('close', function() {
    console.log("Closing")
    clearInterval(hbt)
    client_res = null
  });
})

// Start server listening to client connections
app.listen(client_port, () => console.log(`Server listening on port ${client_port} for client connections`))
