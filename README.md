Comms-GUI
=========

Web GUI designed for use with comms-rs.

![Time Domain Plot](public/images/timeplot.png)

Uses node.js, so make sure you have that installed and then install
dependencies via npm with:

```sh
$ npm install
```

Now you can start the server with a simple:

```sh
$ node app.js
```

And then navigate a browser to `localhost:3000`.  To send it some mock data run
the helper script:

```sh
$ node zmqprod_const.js
```

![Waterfall Plot](public/images/waterfallplot.png)

TODO
----

1. Add ability to plot two things at once on same screen.

2. Add ability to plot from two streams simultaneously.

3. Add ability to attach multiple different plots to the same stream.
