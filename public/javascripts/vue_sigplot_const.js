Vue.component('const-plot', {
  template: '<div></div>',
  data: function() {
    return {
      points: [],
      rt_plot: null,
      evtSource: null,
    }
  },
  methods: {
    init_plot: function() {
      console.log("Loading constellation plot")
      var plot_options = {
        autohide_panbars: true,
        autohide_readout: true,
        hide_note: true
      };
      var data_header = {
        xunits: "Time",
        xstart: 100, // the start of the x-axis
        xdelta: 50, // the x-axis step between each data point
        yunits: "Power"
      };
      var layer_options = {
        name: "Sample Data"
      };
      this.rt_plot = new sigplot.Plot(this.$el, plot_options);
    },
    update_plot: function(new_data) {
      var data_layer = this.rt_plot.get_layer(0);
      var n_points = 500
      if (data_layer) {
        this.rt_plot.push(0, new_data);
        n_points = new_data.len / 2
      } else {
        this.rt_plot.overlay_pipe({
          type: 1000,
          format: "CF"
        }, {
          framesize: n_points,
          line: 0,
          radius: 1,
          symbol: 1
        });
        this.rt_plot.change_settings({
          cmode: 5,
          xmin: -2,
          xmax: 2,
          ymin: -2,
          ymax: 2,
        });
        this.rt_plot.overlay_array(new_data, {file_name: "new_data"});
      }
    },
  },
  mounted: function() {

    console.log("Mounted to DOM")

    this.init_plot()

    this.update_plot();

    // Some BS we have to do for the event listener
    var that = this

    // Set up the EventSource object, add callback for SSE messages
    this.evtSource = new EventSource("/data-server")
    this.evtSource.onmessage = function(event) {
      var data_array = event.data.split(",").map(value => Number(value))
      //console.log(data_array)
      that.update_plot(data_array)
    }
  },
})
