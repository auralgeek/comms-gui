window.onload = function() {
  var plot_options = {
    autohide_panbars: true,
    hide_note: true,
  };
  var data_header = {
    xunits: "Time",
    xstart: 100, // the start of the x-axis
    xdelta: 50, // the x-axis step between each data point
    yunits: "Power"
  };
  var layer_options = {
    name: "Sample Data"
  };
  var rt_plot = new sigplot.Plot(document.getElementById('plot'), plot_options);

  update_rtplot();

  // Set up the EventSource object, add callback for SSE messages
  const evtSource = new EventSource("/data-server");
  evtSource.onmessage = function(event) {
    var data_array = event.data.split(",").map(value => Number(value))
    console.log(data_array);
    update_rtplot(data_array);
  }

  function update_rtplot(new_data) {

    var data_layer = rt_plot.get_layer(0);
    var n_points = 500
    if (data_layer) {
      rt_plot.push(0, new_data);
      n_points = new_data.len / 2
    } else {
      rt_plot.overlay_pipe({
        type: 1000,
        format: "CF"
      }, {
        framesize: n_points,
        line: 0,
        radius: 1,
        symbol: 1
      });
      rt_plot.change_settings({
        cmode: 5,
        xmin: -2,
        xmax: 2,
        ymin: -2,
        ymax: 2,
      });
      rt_plot.overlay_array(new_data, {file_name: "Symbols"});
    }
  }
}
