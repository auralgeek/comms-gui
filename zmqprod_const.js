const zmq = require("zeromq")
const cbor = require("cbor")

// Helper to create Gaussian noise
function gaussian() {
  const N = 20
  var value = 0.0
  for (var i = 0; i < N; i++) {
    value += Math.random()
  }
  return value / N - 0.5
}

// Helper to create random 0's and 1's
function rand_bits(N) {
  var output = []
  for (var i = 0; i < N; i++) {
    if (Math.random() < 0.5) {
      output.push(0)
    } else{
      output.push(1)
    }
  }

  return output
}

// Helper to create a random data array
function mock_qpsk_data(N) {
  var data = rand_bits(2 * N)
  data = data.map(value => 1.0 - 2 * value + gaussian())
  return data
}

async function run() {
  const sock = new zmq.Push

  await sock.bind("tcp://127.0.0.1:57324")
  console.log("Producer bound to port 57324")

  cntr = 0
  while (true) {
    console.log("sending " + cntr)
    cntr += 1
    data = mock_qpsk_data(1000)
    await sock.send(cbor.encode(data.map(value => value.toString()).join(",")))
    await new Promise(resolve => setTimeout(resolve, 16))
  }
}

run()
